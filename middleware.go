package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Middle (c *gin.Context	) {
	fmt.Println("middleware")
	var body_v bodyContent
	if err := c.BindJSON( &body_v); err != nil {
		fmt.Print("error", err.Error())
		return

	}
	if body_v.Name == "Ayele" {
		fmt.Print("not valid")
		c.JSON(http.StatusUnauthorized, gin.H{
			"message":"Unauthorized",
		})
	}
	// c.Set("updated", "added key name")
	// c.Next()
}