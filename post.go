package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

type Response struct {
	Message string 	`json:"message"`
}
type h struct {
	Token string `header:"Authorization"`
}
type Content struct {
	Metadata map[string]interface{}
}
type Book struct {
	metadata map[string]string
}
func PostTest() gin.HandlerFunc {
	return func (c *gin.Context) {

	var body_value bodyContent
	fmt.Println("gin value", c.Request.Body)
	
	defer c.Request.Body.Close()
	
	head := c.GetHeader("Authorization")
	l := strings.Split(head, " ")
	fmt.Println("vvvv",l)

	pubKey, err := ioutil.ReadFile("./publicKey.pem")
	if err != nil {
		fmt.Println("private key error")
		return
	}
	prvtKey, err := ioutil.ReadFile("./privateKey.pem")
	if err != nil {
		fmt.Println("private key error")
		return
	}
 
	jwtToken := NewJWT(prvtKey, pubKey)

	var cont = map[string]interface{}{
		"id": "valu23456789876543",
		"email":"testr@gmail.com",
	}
	
	// 1. Create a new JWT token.
	tok, err := jwtToken.Create(time.Hour,cont)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("TOKEN:", tok)

	// 2. Validate an existing JWT token.
	content, err := jwtToken.Validate(tok)
	if err != nil {
		// log.Fatalln(err)
		fmt.Println("error::", err)
	}
	var cont7 = content
	fmt.Println("CONTENT:: EMAIL", cont7)
	
	// var i interface{} = &Book{cont7["metadata"]}
	
	// cont7["metadata"]
	// var content2 = make(map[string]interface{})
	// content2 = content

	if err := c.BindJSON(&body_value); err != nil {
		// log.Logger("", err.Error())
		fmt.Println("value")
	}
	// if err != nil {
	// 	fmt.Println("invalid format")
	// }
	fmt.Println("gin value", body_value)
	
	c.JSON(http.StatusOK, gin.H{"label": "Hello from mila", "value": body_value})
	}

	// return
}