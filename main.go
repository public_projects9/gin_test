package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/public_projects9/gin_test/handlers"
)
type bodyContent struct {
	Name  string `json:"name"`
	ID string `json:"id"`
}

func main() {
	fmt.Println("From gin Go server .... ")

	r := gin.Default()
   
	// r.POST("/test", handler)
	r.POST("/test",  PostTest())

	r.POST("/test_2", handlers.PostTest())

	// gin.DebugPrintRouteFunc = func(httpMethod, absolutePath, handlerName string, nuHandlers int) {
	// 	log.Printf("endpoint %v %v %v %v\n", httpMethod, absolutePath, handlerName, nuHandlers)
	//   }
	
	r.Run(":8070")
	
}

// func handler_test (c *gin.Context) {
// 	fmt.Println("gin value", c.Request.Body)
// 	return
// }
