package handlers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type bodyContent struct {
	Name  string `json:"name"`
	ID string `json:"id"`
}
type Response struct {
	Message string 	`json:"message"`
}
func PostTest(c *gin.Context) {
	var body_value bodyContent
	fmt.Println("gin value", c.Request.Body)
	// body, err := ioutil.ReadAll(c.Request.Body)
	// real_value := json.Unmarshal(body, &body_value)
	if err := c.BindJSON(&body_value); err != nil {
		// log.Logger("", err.Error())
		fmt.Println("value")
	}
	// if err != nil {
	// 	fmt.Println("invalid format")
	// }
	fmt.Println("gin value", body_value)
	
	c.JSON(http.StatusOK, gin.H{"label": "Hello from mila", "value": body_value})

	// return
}